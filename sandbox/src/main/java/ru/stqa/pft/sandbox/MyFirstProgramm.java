package ru.stqa.pft.sandbox;

class MyFirstProgramm {
    public static void main(String[] args) {
        hello("world");
        hello("user");
        hello("Alexey");

        Square s = new Square(5);
        System.out.println("Площадь квадрата стороной " + s.l + " и " + s.area());

        Rectangle r = new Rectangle(4, 6);
        System.out.println("Площадь прямоугольника со сторонами " + r.a + " и " + r.area());
    }

    public static void hello(String somebody) {
        System.out.println("Hello, " + somebody + "!");
    }
}